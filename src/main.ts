import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';

const PORT = 3000;

async function bootstrap() {
  const app: NestExpressApplication =
    await NestFactory.create<NestExpressApplication>(AppModule);
  await app.listen(PORT);
}

bootstrap().then(() => console.log(`Server is running on PORT : ${PORT}`));
